### Letras Financeiras - Data Visualization

#### Full stack Node.js project.

Displays the production of Letras Financeiras provided by CETIP both graphicaly and table.

Check 'package.json' file for the npm dependencies

#### Stylesheet with Bootstrap 4.0

#### Graph with Highstock (Highchart module)