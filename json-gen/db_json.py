# -*- coding: utf-8 -*-

import sqlite3
import pandas as pd
import numpy as np
import json
import io

con = sqlite3.connect('\\\\path\\db\\letrasfinanceiras\\LF.db')

df = pd.read_sql_query('select * from letrasfinanceiras', con)
df['tx_pond'] = df['TAXA'] * df['VALOR_FINANCEIRO']

datas = df.Mes.unique()
df = df[["TP_INSTR_FINANCEIRO", "Mes", "INDEXADOR", "Faixa_Prazo", "Faixa_Valor", "VALOR_FINANCEIRO", "tx_pond"]]

faixa_prazo = ['Ate 2 anos', 'Ate 3 anos', 'Maior 3 anos']
faixa_valor = ['Ate 300Mil', 'Ate 1MM', 'Ate 100MM', 'Maior que 100MM']

db_json = dict()

for dt in datas:
    indexador = df[(df['TP_INSTR_FINANCEIRO'] == 'LF') & (df['Mes'] == dt)]['INDEXADOR'].unique().tolist()

    idx_dic = dict()
    for idx in indexador:

        fxp_dic = dict()
        for fxp in faixa_prazo:

            fxv_dic = dict()
            for fxv in faixa_valor:
                loop_df = \
                    df.loc[
                        (df['TP_INSTR_FINANCEIRO'] == 'LF') &
                        (df['Mes'] == dt) &
                        (df['INDEXADOR'] == idx) &
                        (df['Faixa_Prazo'] == fxp) &
                        (df['Faixa_Valor'] == fxv), :]

                if len(loop_df) == 0:
                    tx_md = 0
                    vol = 0
                    n_oper = 0
                else:
                    tx_md = np.asscalar(loop_df['tx_pond'].sum() / loop_df['VALOR_FINANCEIRO'].sum())
                    vol = np.asscalar(loop_df['VALOR_FINANCEIRO'].sum())
                    n_oper = np.asscalar(loop_df['VALOR_FINANCEIRO'].count())

                fxv_dic[fxv] = {'tx_media':tx_md, 'vol': vol, 'n_oper': n_oper}

            fxp_dic[fxp] = fxv_dic

        idx_dic[idx] = fxp_dic

    db_json[dt] = idx_dic

with io.open(r'\\path\db\letrasfinanceiras\json\lf.json', 'w', encoding='utf-8') as json_file:
    json.dump(db_json, json_file, ensure_ascii=False, indent=4)