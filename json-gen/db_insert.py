# -*- coding: utf-8 -*-

import sqlite3
import pandas as pd
import numpy as np


def faixa_prazo(x):
    if x == 99999:
        out_str = "LFSC-ERRO"
    elif x <= 730.0:
        out_str = "Ate 2 anos"
    elif x <= 1095.0:
        out_str = "Ate 3 anos"
    else:
        out_str = "Maior 3 anos"
    return out_str


def faixa_valor(x):
    if x <= 300000:
        s = "Ate 300Mil"
    elif x <= 1000000:
        s = "Ate 1MM"
    elif x <= 100000000:
        s = "Ate 100MM"
    else:
        s = "Maior que 100MM"
    return s


con = sqlite3.connect(r'\\path\db\letrasfinanceiras\LF.db')

df = pd.read_excel(r'\\path\db\letrasfinanceiras\mail_xls\MARKET_DATA_LF_LFS.XLSX')
# df = pd.read_excel(r'\\path\db\letrasfinanceiras\xls_base\base.xlsx')

df['DAT_EMISSAO'] = df['DAT_EMISSAO'].apply(lambda x: str(x)[0:10])
df['DAT_VENCIMENTO'] = df['DAT_VENCIMENTO'].apply(lambda x: str(x)[0:10])
df['DAT_REGISTRO'] = df['DAT_REGISTRO'].apply(lambda x: str(x)[0:10])
# New columns
df['Faixa_Prazo'] = np.vectorize(faixa_prazo)(df['PRAZO_DC'])
df['Faixa_Valor'] = np.vectorize(faixa_valor)(df['VALOR_FINANCEIRO'])
df['Mes'] = df['DAT_REGISTRO'].apply(lambda x: str(x)[0:7])

# Check DF
df_check = pd.read_sql_query('select * from letrasfinanceiras where DAT_REGISTRO = "{}"'.format(df['DAT_REGISTRO'][0])
                             ,con)
if len(df_check) > 0:
    print("File already in the database")
else:
    df.to_sql('letrasfinanceiras', con, index=False, if_exists='append')
    print("File insert success")

#df.to_sql('letrasfinanceiras', con, index=False)