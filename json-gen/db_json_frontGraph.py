# -*- coding: utf-8 -*-

import sqlite3
import pandas as pd
import numpy as np
import json
import io

con = sqlite3.connect('\\\\path\\db\\letrasfinanceiras\\LF.db')

df = pd.read_sql_query('select * from letrasfinanceiras', con)
datas = df[df['TP_INSTR_FINANCEIRO'] == 'LF'].Mes.unique()

faixa_prazo = ['Ate 2 anos', 'Ate 3 anos', 'Maior 3 anos']
indexes = df[df['TP_INSTR_FINANCEIRO'] == 'LF']['INDEXADOR'].unique()

front_json = dict()

front_json['categories'] = datas.tolist()

for fx in faixa_prazo:
    vol = []
    for dt in datas:
        vol.append(
            np.asscalar(df[(df['TP_INSTR_FINANCEIRO'] == 'LF') & (df['Faixa_Prazo'] == fx) & (df['Mes'] == dt)]['VALOR_FINANCEIRO'].sum() / 1000000000)
        )
    front_json[fx] = vol

for idx in indexes:
    vol = []
    for dt in datas:
        vol.append(
            np.asscalar(df[(df['TP_INSTR_FINANCEIRO'] == 'LF')& (df['Mes'] == dt) & (df['INDEXADOR'] == idx)]['VALOR_FINANCEIRO'].sum())
        )
    front_json[idx] = vol


with io.open(r'\\path\db\letrasfinanceiras\json\front_graph.json', 'w', encoding='utf-8') as json_file:
    json.dump(front_json, json_file, ensure_ascii=False, indent=4)