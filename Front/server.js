var express = require('express');
var app = express();
var fs = require('fs');

app.use(express.static('public'));

app.get('/database', function(req, res) {
    fs.readFile(__dirname + "/public/json/lf.json", "utf-8", function(err, data) {
        console.log(data);
        res.end(data);
    });
})

app.get('/dbfront', function(req, res) {
    fs.readFile(__dirname + "/public/json/front_graph.json", "utf-8", function(err, data) {
        console.log(data);
        res.end(data);
    });
})

app.get('/LF', function(req, res) {
    res.sendFile(__dirname + '/public/', 'index.html')
})

app.get('/table', function(req, res) {
    res.sendFile(__dirname + '/public/table/','index.html')
})

app.listen(8080, function() {
    console.log('Server on localhost:8080');
})