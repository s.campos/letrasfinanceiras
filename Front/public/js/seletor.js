$(document).ready(function(){
    $.get("http://localhost:8080/database", function(data) {
        var obj = JSON.parse(data);
        var keys = Object.keys(obj);
        var sel = document.getElementById('seletor');
        var fragment = document.createDocumentFragment();

        keys.forEach(function(keys, index) {
            var opt = document.createElement('option');
            opt.innerHTML = keys;
            opt.value = keys;
            fragment.appendChild(opt);
        });
        sel.appendChild(fragment);
        $('select').formSelect();
    });
});