function updateTable() {
    var sel = document.getElementById('seletor')
    $.get("http://localhost:8080/database", function(data) {
        var obj = JSON.parse(data);
        var tbl_vals = obj[sel.value];
        var indexes = Object.keys(tbl_vals);
        
        var content = '';

        indexes.forEach(function(key) {
            content += '<tr>';
            content += '<td style="text-align:center;font-weight: bold;vertical-align: middle !important;" rowspan="4">' + key + '</td>';
            content += '<td style="text-align: center">' + "Até 300Mil" + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Ate 2 anos']['Ate 300Mil']['tx_media'] * 100).toFixed(2) + "%" + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Ate 2 anos']['Ate 300Mil']['vol'] / 1000).toFixed(2) + '</td>';
            content += '<td style="text-align: center">' + tbl_vals[key]['Ate 2 anos']['Ate 300Mil']['n_oper'] + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Ate 3 anos']['Ate 300Mil']['tx_media'] * 100).toFixed(2) + "%" + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Ate 3 anos']['Ate 300Mil']['vol'] / 1000).toFixed(2) + '</td>';
            content += '<td style="text-align: center">' + tbl_vals[key]['Ate 3 anos']['Ate 300Mil']['n_oper'] + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Maior 3 anos']['Ate 300Mil']['tx_media'] * 100).toFixed(2) + "%" + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Maior 3 anos']['Ate 300Mil']['vol'] / 1000).toFixed(2) + '</td>';
            content += '<td style="text-align: center">' + tbl_vals[key]['Maior 3 anos']['Ate 300Mil']['n_oper'] + '</td>';
            content += '</tr>';

            content += '<tr>';
            content += '<td style="text-align: center">' + "Até 1MM" + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Ate 2 anos']['Ate 1MM']['tx_media'] * 100).toFixed(2) + "%" + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Ate 2 anos']['Ate 1MM']['vol'] / 1000).toFixed(2) + '</td>';
            content += '<td style="text-align: center">' + tbl_vals[key]['Ate 2 anos']['Ate 1MM']['n_oper'] + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Ate 3 anos']['Ate 1MM']['tx_media'] * 100).toFixed(2) + "%" + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Ate 3 anos']['Ate 1MM']['vol'] / 1000).toFixed(2) + '</td>';
            content += '<td style="text-align: center">' + tbl_vals[key]['Ate 3 anos']['Ate 1MM']['n_oper'] + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Maior 3 anos']['Ate 1MM']['tx_media'] * 100).toFixed(2) + "%" + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Maior 3 anos']['Ate 1MM']['vol'] / 1000).toFixed(2) + '</td>';
            content += '<td style="text-align: center">' + tbl_vals[key]['Maior 3 anos']['Ate 1MM']['n_oper'] + '</td>';
            content += '</tr>';

            content += '<tr>';
            content += '<td style="text-align: center">' + "Até 100MM" + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Ate 2 anos']['Ate 100MM']['tx_media'] * 100).toFixed(2) + "%" + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Ate 2 anos']['Ate 100MM']['vol'] / 1000).toFixed(2) + '</td>';
            content += '<td style="text-align: center">' + tbl_vals[key]['Ate 2 anos']['Ate 100MM']['n_oper'] + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Ate 3 anos']['Ate 100MM']['tx_media'] * 100).toFixed(2) + "%" + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Ate 3 anos']['Ate 100MM']['vol'] / 1000).toFixed(2) + '</td>';
            content += '<td style="text-align: center">' + tbl_vals[key]['Ate 3 anos']['Ate 100MM']['n_oper'] + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Maior 3 anos']['Ate 100MM']['tx_media'] * 100).toFixed(2) + "%" + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Maior 3 anos']['Ate 100MM']['vol'] / 1000).toFixed(2) + '</td>';
            content += '<td style="text-align: center">' + tbl_vals[key]['Maior 3 anos']['Ate 100MM']['n_oper'] + '</td>';
            content += '</tr>';

            content += '<tr>';
            content += '<td style="text-align: center">' + "Maior que 100MM" + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Ate 2 anos']['Maior que 100MM']['tx_media'] * 100).toFixed(2) + "%" + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Ate 2 anos']['Maior que 100MM']['vol'] / 1000).toFixed(2) + '</td>';
            content += '<td style="text-align: center">' + tbl_vals[key]['Ate 2 anos']['Maior que 100MM']['n_oper'] + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Ate 3 anos']['Maior que 100MM']['tx_media'] * 100).toFixed(2) + "%" + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Ate 3 anos']['Maior que 100MM']['vol'] / 1000).toFixed(2) + '</td>';
            content += '<td style="text-align: center">' + tbl_vals[key]['Ate 3 anos']['Maior que 100MM']['n_oper'] + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Maior 3 anos']['Maior que 100MM']['tx_media'] * 100).toFixed(2) + "%" + '</td>';
            content += '<td style="text-align: center">' + parseFloat(tbl_vals[key]['Maior 3 anos']['Maior que 100MM']['vol'] / 1000).toFixed(2) + '</td>';
            content += '<td style="text-align: center">' + tbl_vals[key]['Maior 3 anos']['Maior que 100MM']['n_oper'] + '</td>';
            content += '</tr>';    
        });
        
        $('#main_tab tbody').html(content);
    })
}